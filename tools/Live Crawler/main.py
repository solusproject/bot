import praw
import re

reddit = praw.Reddit(
)

def parseComments(comment):
    # Remove parts we don't want/need
    comment.body = str(comment.body).replace('\n', '')
    comment.body = re.sub("u/[a-zA-Z]+", "", str(comment.body))
    comment.body = re.sub("r/[a-zA-Z]+", "", str(comment.body))
    comment.body = re.sub("http\S+", "", str(comment.body))
    comment.body = str(comment.body).replace('�', '')
    comment.body = str(comment.body).replace('��', '')
    # Remove entire comments we don't want/need
    if len(comment.body) < 5:
        return
    elif len(comment.body) > 100:
        return
    elif comment.body == '[deleted]':
        return
    elif comment.body == '[removed]':
        return
    elif '&#x200b' in comment.body:
        return
    else:
        # Save the comment
        messages = open("messages.csv", "a")
        messages.write('"' + str(comment.body) + '"' + "\n")
        messages.close()


subreddit = reddit.subreddit("all")
for comment in subreddit.stream.comments(skip_existing=True):
    if comment.submission.subreddit.over18:
        parseComments(comment)