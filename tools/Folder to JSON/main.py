from os import listdir
import codecs

for i in range(6):
    files = listdir(f"training/{i}")
    for file in files:
        with open(f'training/{i}/' + file) as f:
            dataset = codecs.open('dataset.json', 'w', 'utf-8')
            data = json.load(dataset)
            data[i].append(f)
            json.dump(data, dataset)