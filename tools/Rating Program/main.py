import csv
import os
import tkinter as tk


class App():
    def __init__(self):
        # Make sure all directories are active
        try:
            os.mkdir("training")
            os.mkdir("training/0")
            os.mkdir("training/1")
            os.mkdir("training/2")
            os.mkdir("training/3")
            os.mkdir("training/4")
            os.mkdir("training/5")
        except FileExistsError:
            print("Config is done")

        # Start GUI
        self.root = tk.Tk()
        w, h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
        self.root.geometry("%dx%d+0+0" % (w, h))
        self.message = tk.Label(text="", font=('Courier'))
        self.message.pack(side="top", pady=40)

        # List all buttons
        self.level_0 = tk.Button(self.root, text="0", command=self.level_0)
        self.level_0.pack()
        self.level_1 = tk.Button(self.root, text="1", command=self.level_1)
        self.level_1.pack()
        self.level_2 = tk.Button(self.root, text="2", command=self.level_2)
        self.level_2.pack()
        self.level_3 = tk.Button(self.root, text="3", command=self.level_3)
        self.level_3.pack()
        self.level_4 = tk.Button(self.root, text="4", command=self.level_4)
        self.level_4.pack()
        self.level_5 = tk.Button(self.root, text="5", command=self.level_5)
        self.level_5.pack()
        self.level_p = tk.Button(self.root, text="Pass", command=self.level_p)
        self.level_p.pack()
        self.back = tk.Button(self.root, text="Go Back", command=self.back)
        self.back.pack(pady=20)

        # Get the last rated message
        self.message_id = 0
        with open('.cache', newline='') as last_message:
            self.message_id = int(last_message.read()) + 1
            last_message.close()

        # Get the dataset
        with open('messages.csv', newline='', encoding='utf-8') as dataset:
            self.message_dataset = list(csv.reader(
                dataset, delimiter=',', quotechar='"'))

        self.message.config(text=self.message_dataset[self.message_id][0])
        self.root.mainloop()

    def rate(self, rating):
        with open(f"training/{rating}/"+str(self.message_id), 'w', newline='') as output:
            output.write(self.message_dataset[self.message_id][0])
            output.close()
        self.message_id = self.message_id + 1
        self.message.config(text=self.message_dataset[self.message_id][0])
        with open(".cache", 'w', newline='') as output:
            output.write(str(self.message_id))
            output.close()

    def level_0(self):
        self.rate(0)

    def level_1(self):
        self.rate(1)

    def level_2(self):
        self.rate(2)

    def level_3(self):
        self.rate(3)

    def level_4(self):
        self.rate(4)

    def level_5(self):
        self.rate(5)

    def level_p(self):
        self.message_id = self.message_id + 1
        self.message.config(text=self.message_dataset[self.message_id][0])
        with open(".cache", 'w', newline='') as output:
            output.write(str(self.message_id))
            output.close()

    def back(self):
        self.message_id = self.message_id - 1
        try:
            os.remove("training/0/" + str(self.message_id))
        except FileNotFoundError:
            pass
        try:
            os.remove("training/1/" + str(self.message_id))
        except FileNotFoundError:
            pass
        try:
            os.remove("training/2/" + str(self.message_id))
        except FileNotFoundError:
            pass
        try:
            os.remove("training/3/" + str(self.message_id))
        except FileNotFoundError:
            pass
        try:
            os.remove("training/4/" + str(self.message_id))
        except FileNotFoundError:
            pass
        try:
            os.remove("training/5/" + str(self.message_id))
        except FileNotFoundError:
            pass
        self.message.config(text=self.message_dataset[self.message_id][0])


app = App()
