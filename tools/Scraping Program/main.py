import praw
import prawcore
import re

reddit = praw.Reddit(
)

subreddits = []
with open('subreddits.txt', newline='') as subreddits_on_file:
    subreddits = subreddits_on_file.readlines()

def comment_gather(submission):
    submission.comments.replace_more(limit=None)
    for comment in submission.comments.list():
        try:
            # Remove parts we don't want/need
            comment.body = str(comment.body).replace('\n', '')
            comment.body = re.sub("u/[a-zA-Z]+", "", str(comment.body))
            comment.body = re.sub("r/[a-zA-Z]+", "", str(comment.body))
            comment.body = re.sub(
                "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))", "", str(comment.body))
            comment.body = str(comment.body).replace('�', '')
            comment.body = str(comment.body).replace('��', '')

            # Remove entire comments we don't want/need
            if len(comment.body) < 5:
                continue
            elif len(comment.body) > 100:
                continue
            elif comment.body == '[deleted]':
                continue
            elif comment.body == '[removed]':
                continue
            elif '&#x200b' in comment.body:
                continue
            else:
                # Save the comment
                messages = open("messages.csv", "a")
                messages.write('"' + str(comment.body) + '"' + "\n")
                messages.close()
        except:
            pass

for subreddit in subreddits:
    try:
        for submission in reddit.subreddit(subreddit).new():
            print(submission.title)
            comment_gather(submission)
    except BaseException:
        pass

