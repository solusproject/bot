# Quinn Bot

This software combines Quinn's AI with the platforms that want to use Quinn. This software enables community managers to add Quinn's functionality and empower their community with tools to prevent pedophilia.

## Usage

You will need to run the following commands to use all of Quinn:

```bash
git clone git@gitlab.com:solusproject/bots.git
cd /bots |
npm start
```

This runs the bot itself, and since it has direct hooks into the model, it doesn't need to use the AI API.

## Support

For code support, contact developers@solusproject.org

For questions or concerns about the Solus Project, contact support@solusproject.org

## Contributing

We are always open to contributions! If you feel like you can add something, feel free to make a pull request and we'll have our team of caffeinated developers review it! We love seeing our community come together to make the Solus Project what it is.

## Authors and Acknowledgements

Head Developer: William Kenzie

## License

All of our projects use the MIT License, which means you can remix or redistribute what we make however you see fit, as long as you acknowledge that you used our code.

Content like images are always licensed under the CC-BY-SHA license unless stated otherwise.

